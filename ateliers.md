---
layout: ateliers.mustache
title: Les ateliers
description: Les ateliers proposés par Dominique Duprey.
cover:
  alt: Une main qui écris
  legend: >
    N'écris que ce qui chante en toi — Sergueï Rachmaninov  
    
    Photo par [Negative Space](https://www.pexels.com/@negativespace)

  src: medias/uploads/ateliers-couverture.jpg
---

## Les ateliers se déroulent en 3 temps

  1. **Un temps pour stimuler votre imaginaire** :  
  Je vous envoie par e-mail (ou je vous lis) quelques courts extraits d'un livre sur un thème donné et je le commente.

  2. **Un temps pour écrire** :  
  C′est à vous de prendre la plume autour d′une proposition qui va vous porter dans votre propre texte, et je suis là pour vous aider personnellement si besoin, pas de page blanche !

  3. **Un temps de partage** :  
   Chacun commente le texte des membres du groupe, à la fois sur la forme et sur le fond, dans le profond respect du travail de tous.
