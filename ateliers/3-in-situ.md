---
title: In situ 
date: 2019-01-01
price:
  name: Prendre contact
  path: a-propos#contact
---

Vous avez un groupe d’amis intéressés par l’écriture. Contactez-moi, nous organiserons **un atelier dans un lieu qui sera possible pour tous**.

Vous aurez alors une proposition, vous écrivez sur place, et nous commentons ensemble vos écrits sur la forme et sur le fond. C’est très stimulant et formateur !
