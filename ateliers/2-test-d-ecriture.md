---
title: Test d'écriture
price:
  name: Atelier Test d'écriture
  price: 10€
  link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=28PS6DSX6GPFC
date: 2019-02-01
---

Vous aimez écrire, mais vous n’avez pas encore fait le pas réellement. Ce test est pour vous ! Je vous envoie une proposition d’écriture qui va vous servir de tremplin.

**Vous écrivez une ou deux pages, puis vous avez mon retour**. Cela vous permet de voir vos points forts, et comment vous améliorer.

