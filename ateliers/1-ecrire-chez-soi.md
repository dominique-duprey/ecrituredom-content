---
title: Écrire chez soi
price:
  name: Atelier Écrire chez soi
  price: 20€
  link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=6NBXKY227J7K6
date: 2019-03-01
---

Vous souhaitez progresser et être stimulé dans votre écriture.

Je vous envoie tous les premiers du mois **une proposition d’écriture par mail**, vous avez jusqu’au 25 du mois pour envoyer votre écrit au groupe. Puis, chacun fait un retour sur le fond et sur la forme, une très belle façon d’avancer !
