---
title: À propos
description: Dominique Duprey est écrivain et animateur d'atelier d'écriture.
---
<article class="
columns-medium-medium
">
  <div class="column-two-thirds">
    <header>
      <h2>Dernier livre</h2>
      <h1>Mon p'tit tarot</h1>
    </header>
    <p>
      Voici le Tarot de Marseille, jeu initiatique, pas à pas, pour les nuls comme pour les initiés !
    </p>
    <p>
      <strong>Le livre de 164 pages est un guide pratique</strong>, joyeux et simple ; <strong>Le jeu de 22 cartes</strong> est d’une beauté étonnante, proche des mangas.
    </p>
    <p>
      Le tout est une plongée jubilatoire, une rencontre avec un ami qui vous veut du bien !
    </p>
  </div>
  <div class="column-third">
    <figure>
      <img src="/medias/uploads/livre-couverture-mon-ptit-tarot.jpg" alt="Couverture du livre &laquo;Mon p'tit tarot&raquo;">
    </figure>
    <p>
      <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=PL9V635R9BN4J" class="button is-strong">Acheter le livre et le jeu 35€</a>
    </p>
  </div>
</article>

<div class="columns-auto">
  <aside>
    <!-- @todo: à place dans un collection éditable -->
    <h2>L'auteur</h2>
    <figure>
      <img src="medias/uploads/portrait-dominique-duprey.jpg">
      <figcaption>
        <p>Dominique Duprey</p>
      </figcaption>
    </figure>
    <p>
      Je suis écrivain et animateur d’ateliers d’écriture.
    </p>
    <p>
      Mon expérience m’a montré que l’écriture et la lecture avaient des pouvoirs tout à fait extraordinaires. Elles réveillent la créativité, l’imaginaire, et mettent du piment dans la vie ! En fait, c’est la plume qui vous guide et qui fait le travail avec et pour vous !
    </p>
    <p>
      <a href="dominique-duprey" class="button">En savoir plus sur l'auteur</a>
    </p>
  </aside>
  <aside id="contact">
    <h2>Contacts</h2>
    <ul class="is-soft">
      <li>Tél: 06 08 57 58 98</li>
      <li><a href="mailto:contact@ecrituredom.com">contact@ecrituredom.com</a></li>
    </ul>
  </aside>
</div>
