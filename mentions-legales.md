---
title: Mentions Légales
---
[comment]: <> (cf. : https://www.service-public.fr/professionnels-entreprises/vosdroits/F31228)

[comment]: <> (
Marqueurs à copier/remplacer :
http://ecrituredom.com  
Dominique Duprey  
SIRET 95062133400032
23 rue du Mont  
76000 Rouen  
France  
06 08 57 58 98  
contact@ecrituredom.com  
http://ecrituredom.com  
Ziopod  
36, rue Molière  
76000 Rouen  
France  
06 78 30 87 13  
steve.beau@ziopod.com  
http://ziopod.com  
[HOST NAME]  
[HOST ADDRESS]  
[HOST PHONE]  
[HOST EMAIL]  
[HOST WEBSITE]  
)


# Mentions légales

## Informations éditeur

**Dominique Duprey**  
SIRET 95062133400032
23 rue du Mont  
76000 Rouen  
France  
06 08 57 58 98  
contact@ecrituredom.com  
http://ecrituredom.com  

## Informatique et liberté

En tant qu’utilisateur, vous disposez gratuitement d’un droit d’accès, de modification et de suppression de votre contribution à ce site internet. Pour cela, vous pouvez adresser un courrier ou un Email à :

**Dominique Duprey**  
SIRET 95062133400032
23 rue du Mont  
76000 Rouen  
France  
06 08 57 58 98  
contact@ecrituredom.com  
http://ecrituredom.com  

### Licence de droits et d’usage et copyright

En application du code français de la propriété et plus généralement des traités et accords internationaux comportant des dispositions relatives à la protection des droits d’auteurs, il est formellement interdit de reproduire pour un usage autre que privé, mais aussi de vendre, distribuer, émettre, diffuser, adapter, modifier, publier, communiquer intégralement ou partiellement, sous quelque forme que ce soit, les données du site http://ecrituredom.com sans l'accord de Dominique Duprey

Les copyrights et droits des visuels du site sont les suivants : Tous droit réservés Dominique Duprey.

### Création de liens :

L’établissement de liens est autorisé aux conditions cumulatives suivantes :

 - Que le lien apparaisse sous l'appellation suivante : "http://ecrituredom.com" Dominique Duprey
 - Que le site de Dominique Duprey s'ouvre dans une nouvelle fenêtre, ou en remplacement de la fenêtre ouverte. En aucun cas le site de Dominique Duprey ne devra s'ouvrir à partir d'une "frame" ou toute autre dispositif réduisant l'écran ; Dans tous les cas, les liens directs vers les sources de documentations et pages profilées sont formellement interdits.

### Limitation de responsabilité

Dominique Duprey se réserve le droit de modifier ou de corriger le contenu de son site à tout moment, sans préavis. Dominique Duprey ne pourra être tenu pour responsable en cas de contamination des matériels informatiques des internautes résultant de la propagation d'un virus ou autres infections informatiques. Il appartient à l'utilisateur de ce site de prendre toutes les mesures appropriées de façon à protéger ses propres données et/ou logiciels de la contamination par des éventuels virus circulant sur le réseau Internet. En aucun cas Dominique Duprey, ses employés ou les tiers mentionnés dans son site ne pourront être tenus responsables, au titre d'une action en responsabilité contractuelle, en responsabilité délictuelle ou de tout autre action, de tout dommage direct ou indirect, incident ou accessoire, ou de quelque nature qu'il soit ou de tout préjudice, notamment, de nature financier, résultant de l'utilisation de son site ou d'une quelconque information obtenue sur son site.

## Conception et réalisation

**Ziopod** Steve Beau  
Localhost  
36, rue Molière  
76000 Rouen  
FRANCE  
Port. 06 78 30 87 13  
<steve.beau@ziopod.com>  
<http://ziopod.com>  

## Hébergement

**[HOST NAME]**  
[HOST ADDRESS]  
[HOST PHONE]  
[HOST EMAIL]  
[HOST WEBSITE]  
