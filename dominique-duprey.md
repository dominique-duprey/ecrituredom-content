---
title: Biographie de Dominique Duprey
---

<div class="columns" data-markdown>
<div class="column-third">
<figure>
  <img src="medias/uploads/portrait-dominique-duprey.jpg">
  <figcaption>
    <p>Dominique Duprey</p>
  </figcaption>
</figure>

# Dominique Duprey

Je suis écrivain et animateur d’ateliers d’écriture.

Mon expérience m’a montré que l’écriture et la lecture avaient des pouvoirs tout à fait extraordinaires. Elles réveillent la créativité, l’imaginaire, et mettent du piment dans la vie ! En fait, c’est la plume qui vous guide et qui fait le travail avec et pour vous !

A ce jour, j’ai écrit plusieurs livres que je vous présente dans ce site et j’ai grand plaisir à animer des ateliers d’écriture sur la toile, mais aussi chez moi.

Ma fibre de pianiste et thérapeute nourrissent mon expérience autour des livres. J’aime faire partager le goût de la plume, et j’aime aider les textes à accoucher d’eux-mêmes.

C’est une aventure passionnante, toujours renouvelée et qui ne cesse de m’émerveiller !
</div>
<div class="column-two-thirds">

## Bibliographie

  > « Ecrire. Je ne peux pas. Personne ne peut. Il faut le dire, on ne peut pas. Et on écrit ! »

  — Marguerite Duras

C’est ainsi que fonctionnent mes ateliers d’écriture. Le miracle se produit toujours. Ils sont des rendez-vous avec vous-même.

Mais c’est aussi ainsi que j’ai écrit et publié plusieurs livres, et que j’en ai toujours un en chantier !

Ecrire m’est une nécessité vitale, tout comme respirer !

**[Rosas](livre#rosas)** est un livre quasi-autobiographique : il met en conte le roman de ma vie, véritable fleuve tumultueux… et joyeux !

**[L’enfant miroirs](livres#l-enfant-miroirs)**, c’est vous, c’est moi : tout enfant possède une capacité de médium, de perception de ce qui ne se voit pas, de ce qui ne se dit pas. Ce livre raconte ce qui se passe derrière le rideau de toute vie !

**[La Barque flamboyante](livres#la-barque-flamboyante)** est un roman époustouflant : Annah, l’héroïne traverse sa vie de concertiste, pianiste avec une virtuosité surprenante. Bien des fois, elle tombe pour mieux se relever et se révéler à elle-même !

**[Soleil de Lune](livres#soleil-de-lune)** est l’histoire de la voix à travers une vie. Voix parlée, voix tue, voix chantée ! Tout un programme à réinventer à chaque instant !

**[Mon P’tit Tarot](livres#mon-ptit-tarot)**, un ami qui vous veut du bien, est un guide simple, à la portée de tous, autour du Tarot de Marseille, ces cartes qui nous parlent de notre vie et de notre devenir…

<a href="livres" class="button is-strong">Découvrir les livres de Dominique Duprey</a>

## Les ateliers

Ce qui m’est le plus demandé et qui marche à merveille :

l’atelier d’écriture une fois par mois par mail. Tous les premiers du mois, je fais une proposition d’écriture au groupe autour d’un livre, véritable tremplin pour prendre soi-même la plume ! Les personnes inscrites à l’atelier, dès qu’elles ont écrit une page ou plus, l’envoient au groupe, et chacun commente et sur la forme, et sur le fond.

Ce procédé est très stimulant, votre écriture devient alors fluide et de plus en plus riche.

J’ai aussi des ateliers à domicile que je développe à la demande : on se réunit chez vous, ou l’un des membres du groupe, et on vit l’expérience de l’écriture ensemble pendant trois heures. C’est dense, intime et puissant !

<!-- [Découvrir les ateliers](/ateliers){.button} -->
<a href="ateliers" class="button is-strong">Découvrir les ateliers</a>
</div>
</div>
