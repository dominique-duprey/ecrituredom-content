---
layout: welcome.mustache
title: Découvrir l'écriture
cover:
  alt: Peinture de ALix, d'une fille aux papillons
  legend: >
    "Pluie d'étoiles" — peinture par [Alix](https://www.facebook.com/alix.tassart)
  src: medias/uploads/accueil-couverture.jpg
---

Vous rêvez d’écrire, mais vous reportez toujours… Ce site est là pour vous aider à faire le premier pas, et à [plonger dans l’univers passionnant de l’écriture !](ateliers).

Vous y découvrirez aussi [mes livres](livres), romans initiatiques. L’humour s’y mêle à la profondeur.
