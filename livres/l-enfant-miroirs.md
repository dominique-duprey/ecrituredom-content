---
title: L'Enfant Miroirs
description: | 
  Gabriel est un enfant discret, presque trop effacé, ce qui va lui attirer quelques déboires. Pourtant, il perçoit toutes les ambiances dans une acuité de conscience surprenante, ce que les adultes ne soupçonnent même pas. Il chemine ainsi jusqu’à devenir  un homme sensible, sensuel, amoureux de la vie. Parcours initiatique avec comme complices la Musique… et l’Amour.
cover:
  alt: Couverture du livre « l'Enfant Miroirs »
  legend: L'enfant miroirs
  src: medias/uploads/livre-couverture-l-enfant-miroirs.jpg
button:
  name: Acheter
  price: 21€
  link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=NBXSRHM8UP5BG
isbn: 978-2-914211-61-1
publish_date: 2008-05-29
publisher: première publication chez Gunten (2008)
price: 21€ TCC (frais de port inclus)
---

Dans cet ouvrage, Dominique Duprey raconte la vie de Gabriel, enfant puis adulte, sur un chemin chaotique entre névrose et guérison. La mort de la mère, au début du roman, met au monde l'adulte. Dominique Duprey emboîte le pas de ce personnage dans lequel il se fond avec sincérité et pudeur. Ses phrases coulent vers un « lac d'amour ».
