---
title: Soleil de Lune
description: | 
  Soleil de lune, c’est la rencontre de ces deux astres dans notre vie la plus intime : Ombre et Lumière ! Annah et Dominique, couple mystérieux et improbable, cheminent au long de cette histoire d’amour autour de la voix. Les vieux fantômes remontent pour mieux laisser place à une explosion de vie. Un livre époustouflant !
cover:
  alt: Couverture du livre « Soleil de Lune »
  legend: Soleil de Lune
  src: medias/uploads/livre-couverture-soleil-de-lune.jpg
button:
  name: Acheter
  price: 23€
  link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=RWL3M8R7ML8PY
upcomming: false
publish_date: 2019-09-01
publisher: Éditer à compte d'auteur
isbn:
price: Le livre, 23€ TTC
---


