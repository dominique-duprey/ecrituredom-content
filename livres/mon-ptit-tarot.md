---
title: Mon P'tit Tarot
description: |
  Voici le Tarot de Marseille, jeu initiatique, pas à pas, pour les nuls comme pour les initiés !

  **Le livre de 164 pages est un guide pratique**, joyeux et simple ; **Le jeu de 22 cartes** est d’une beauté étonnante, proche des mangas.

  Le tout est une plongée jubilatoire, une rencontre avec un ami qui vous veut du bien !

cover:
  alt: Couverture du livre « Mont p'tit tarot »
  legend: Mon p'tit tarot, le livre et le jeu de tarot
  src: medias/uploads/livre-couverture-mon-ptit-tarot.jpg
button:
  name: Acheter le livre et le jeu de cartes
  price: 35€
  link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=PL9V635R9BN4J
publish_date: 2019-07-10
isbn: 9782353213894
pages: 164 pages, 22 cartes
publisher: Scripta, 2019
price: le livre et jeu de cartes 35€ TCC (frais de port inclus)
---

Alors qu’il existe de nombreux ouvrages autour du Tarot de Marseille, jeu de cartes initiatique, j’ai voulu ici offrir à tout public un livre et un jeu d’aujourd’hui, simple, dynamisant, ludique, et en même temps très enseignant.

Que vous soyez initié ou non, ce livre et ce jeu sauront vous parler tout en vous apprenant la signification des symboles, ou en vous invitant à les revisiter.

Simple, séduisant, taquin, ces deux objets précieux et discrets, dans votre sac, dans votre poche, sauront vous ravir de leur présence vibrante.

Fi des cartes qui vous faisaient peur ! Deux outils indispensables pour mieux vivre intensément votre vie.

À la portée de tous. Magnifiques, surprenants et tellement parlants !
