---
title: La barque flamboyante
description: | 
  Annah est une jeune pianiste, concertiste. Sa sensibilité est telle qu’elle fait corps avec son piano, la nature et les hommes, jusqu’à devenir elle-même, après bien des tribulations. Elle sortira de ces épreuves grandie, riche d’une expérience de vie hors du commun. Sa barque n’en sera que plus flamboyante !
cover:
  alt: Couverture du livre « La barque flamboyante »
  legend: La barque flamboyante
  src: medias/uploads/livre-couverture-la-barque-flamboyante.jpg
button:
  name: Acheter
  price: 23€
  link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=HPUDZ7WM4ZDMA
isbn: 9782953448504
publisher: Physalide, 2009
publish_date: 2009-06-01
pages: 237
price: 23€ TCC (frais de port inclus)
---
