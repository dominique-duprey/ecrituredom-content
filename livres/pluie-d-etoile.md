---
title: Pluie d'étoile
description: |
  Je vous présente mon sixième livre, qui, par ces temps troublés et troublants est un véritable baume de Joie et de Profondeur.

  Exit le confinement intérieur ! Ce livre est un guide précieux pour sortir de nos limitations, mentales, psychiques, énergétiques. Accessible à tout un chacun.
cover:
  alt: Couverture du livre « Pluie d'étoile »
  legend: Pluie d'étoile
  src: medias/uploads/livre-couverture-pluie-d-etoile.jpg
button: 
  name: Acheter
  price: 29,90€
  link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=G6ZFSCABPNW3J
publish_date: 2020-04-01
isbn: 9791026248743
pages: 526 pages
publisher: Librinova, 2020
price: le livre 29,90€ TTC (frais de port inclus)
---
